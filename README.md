# The Full Stack Dev Portfolio of Sarah Salvatore

Hello! Thank you for your interest in my Full Stack Dev Portfolio. Please continue reading for more details:

## SITE URL:

LIVE DEMO: [http://sarah-salvatore.com](http://sarah-salvatore.com)

## About

Created to display my programming skills to potential employers, colleagues and clients. The full project consists of three repos:

1. Frontend: HTML, CSS, React, Javascript
2. Backend: Node, Express
3. Server: MySQL

## Launch

The frontend application has been launched using GCP Cloud Storage Bucket.
The backend has been launched using GCP

## CI/CD

A pipeline has been optimized via a Gitlab yaml file to allow for continuous integration and deployment.

## Frontend

The frontend includes the following pages/sections:

### Home Page

- About - Brief description, photo, resume and social links.
- Skills - List of language, frameworks and technical proficiencies.
- Projects/More Projects - Projects built to date.
- Contact - Form and details. Forms are posted to database and emailed on submit.

### Blog Page

- Coming soon. Will include articles geared to help new developers. Topics will include items that I found difficult to learn myself.

### Login Page / Admin Panel / CRUD Pages

- Protected routes that include CRUD operations for contact form entries and admin users.

### Error Page

- Default error page.

## Contributions

Contributions and forks are always welcome! If you have a suggestion that would make my portfolio better, please feel free to fork the repo and create a pull request, or simply send me an email with your suggestions. Don't forget to give this project a star on GitHub!

## Contact

Please find my contact details below. Feel free to reach out anytime.

- Email: sarah.h.salvatore@gmail.com
- Linkedin: [https://www.linkedin.com/in/sarah-salvatore-full-stack-developer/](https://www.linkedin.com/in/sarah-salvatore-full-stack-developer/)
- GitHub: [https://github.com/SarahSalvatore](https://github.com/SarahSalvatore)

## Acknowledgements

- Unsplash [https://unsplash.com/](https://unsplash.com/)
- Font Awesome Icons [https://fontawesome.com/icons](https://fontawesome.com/icons)
- EmailJS [https://www.emailjs.com/](https://www.emailjs.com/)
- Swiper [https://swiperjs.com/](https://swiperjs.com/)
- Google Fonts [https://fonts.google.com/](https://fonts.google.com/)
