import Project1Preview from "./Assets/Images/project1preview.webp";
import Project2Preview from "./Assets/Images/project2preview.jpg";
import Project3Preview from "./Assets/Images/project3preview.webp";

const ProjectList = [
  {
    id: 1,
    title: "Placeholder Project One",
    image: Project1Preview,
    stack: ["#JavaScript", "#React", "#Node", "#Express", "#MySQL"],
    alt: "project one preview",
    description:
      "Here is my description of the project. This will include a brief description outlining what it is, what it does, what programs/stack was used, as well as any cool or interesting facts about it. Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, voluptas! Minus magni fugiat pariatur veniam quis, nihil qui impedit mollitia.",
    stackUsed: "HTML, CSS, Javascript, React, Node, Express",
    demo: "https://www.google.com/",
    repo: "https://github.com/SarahSalvatore",
  },

  {
    id: 2,
    title: "Placeholder Project Two",
    image: Project2Preview,
    stack: ["#TypeScript", "#React", "#Node", "#Express", "#MySQL", "#Sass"],
    alt: "project two preview",
    description:
      "Here is my description of the project. This will include a brief description outlining what it is, what it does, what programs/stack was used, as well as any cool or interesting facts about it. Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, voluptas! Minus magni fugiat pariatur veniam quis, nihil qui impedit mollitia.",
    stackUsed: "HTML, CSS, Javascript, React, Node, Express",
    demo: "https://www.google.com/",
    repo: "https://github.com/SarahSalvatore",
  },

  {
    id: 3,
    title: "Placeholder Project Three",
    image: Project3Preview,
    stack: ["#JavaScript", "#React", "#Node", "#Express", "#MySQL"],
    alt: "project three preview",
    description:
      "Here is my description of the project. This will include a brief description outlining what it is, what it does, what programs/stack was used, as well as any cool or interesting facts about it. Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, voluptas! Minus magni fugiat pariatur veniam quis, nihil qui impedit mollitia.",
    stackUsed: "HTML, CSS, Javascript, React, Node, Express",
    demo: "https://www.google.com/",
    repo: "https://github.com/SarahSalvatore",
  },
];

export default ProjectList;
