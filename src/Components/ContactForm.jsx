import React, { useState, useEffect, useRef } from "react";
import SocialLinks from "./SocialLinks";
import axios from "axios";
import emailjs from "@emailjs/browser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ContactForm = ({ formSubmitted }) => {
  const form = useRef();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [nameError, setNameError] = useState("*");
  const [emailError, setEmailError] = useState("*");
  const [messageError, setMessageError] = useState("*");

  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  useEffect(() => {
    if (!nameError.length && !emailError.length && !messageError.length) {
      axios
        .post(`https://cloud-example-7dgxvgl46q-pd.a.run.app/entries`, {
          name: name,
          email: email,
          message: message,
        })
        .then((response) => {
          formSubmitted(true);
          emailjs.sendForm(
            "service_vqc2cez",
            "template_loop4ok",
            form.current,
            "qsWMunBnCcmSLUPnd"
          );
        })
        .catch((error) => {
          if (error) {
            alert(`Error: ${error.response.data.error}`);
          }
        });
    }
  }, [nameError, emailError, messageError]);

  const checkNameErrors = (name) => {
    if (!name) {
      setNameError("*Please enter your name.");
    } else if (name.length < 2) {
      setNameError("*Name must be at least 2 characters.");
    } else if (name.length > 64) {
      setNameError("*Name must be less than 64 characters.");
    } else {
      setNameError("");
    }
  };

  const checkEmailErrors = (email) => {
    if (!email) {
      setEmailError("*Please enter your email address.");
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError("*Please enter a valid email address.");
    } else if (email.length > 64) {
      setEmailError("*Too long! Maximum email size (64 characters) exceeded.");
    } else {
      setEmailError("");
    }
  };

  const checkMessageErrors = (message) => {
    if (!message) {
      setMessageError("*Please enter a valid message.");
    } else if (message.length < 3) {
      setMessageError("*Message must be at least 4 characters.");
    } else if (message.length > 1063) {
      setMessageError("*Message must be less than 1064 characters.");
    } else {
      setMessageError("");
    }
  };

  const submitContactForm = (event) => {
    event.preventDefault();
    checkNameErrors(name);
    checkEmailErrors(email);
    checkMessageErrors(message);
  };

  return (
    <div className="form-container">
      <form id="form" ref={form} onSubmit={submitContactForm}>
        <div className="input-control">
          <label className="input-label" htmlFor="nameEntry">
            Name:
          </label>
          <input
            id="nameEntry"
            type="text"
            name="name"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
          {nameError.length > 1 ? (
            <p className="error-message">{nameError}</p>
          ) : (
            <p></p>
          )}
        </div>
        <div className="input-control">
          <label className="input-label" htmlFor="emailEntry">
            Email:
          </label>
          <input
            id="emailEntry"
            type="text"
            name="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
          {emailError.length > 1 ? (
            <p className="error-message">{emailError}</p>
          ) : (
            <p></p>
          )}
        </div>
        <div className="input-control">
          <label className="input-label" htmlFor="messageEntry">
            Message:
          </label>
          <input
            id="messageEntry"
            type="textarea"
            name="message"
            value={message}
            onChange={(event) => setMessage(event.target.value)}
          />
          {messageError.length > 1 ? (
            <p className="error-message">{messageError}</p>
          ) : (
            <p></p>
          )}
        </div>

        <button className="submit-button" type="submit">
          <FontAwesomeIcon icon="paper-plane" className="button-icon" />
          Submit
        </button>
      </form>
      <div className="contact-socials">
        <SocialLinks className="contact-social-links" />
      </div>
    </div>
  );
};

export default ContactForm;
