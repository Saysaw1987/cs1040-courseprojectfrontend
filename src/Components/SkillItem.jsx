import React from "react";

const SkillItem = (props) => {
  return (
    <div className="skill-item">
      <img
        className="skill-logo"
        src={props.icon}
        alt={props.alt}
        title={props.title}
      />
      <h1 className="language-header">{props.title}</h1>
    </div>
  );
};

export default SkillItem;
