import React from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import background from "../Assets/Images/3d-small.jpg";

const Blog = () => {
  let navigate = useNavigate();

  return (
    <div
      className="blog-section-container"
      style={{ backgroundImage: `url('${background}')` }}
    >
      <h1 className="blog-section-title">Check Out My Blog</h1>
      <button className="blog-button" onClick={() => navigate("/blog")}>
        <FontAwesomeIcon icon="arrow-right" className="blog-icon" />
      </button>
    </div>
  );
};

export default Blog;
