import React, { useEffect } from "react";
import { HashLink } from "react-router-hash-link";
import { useAnimation, motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import "../CSS/App.css";

const textAnimationVariant = {
  visible: {
    x: 0,
    opacity: 1,
    transition: { duration: 1.2 },
  },
  hidden: {
    x: -50,
    opacity: 0,
    transition: { duration: 1.2 },
  },
};

const Hero = (props) => {
  const controls = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.5 });

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    <section
      className="hero__container"
      style={{ backgroundImage: `url('${props.onToggleChange}')` }}
    >
      <motion.div
        ref={ref}
        animate={controls}
        initial="hidden"
        variants={textAnimationVariant}
        className="hero__text"
      >
        <h1 className="my-name-text">Hello.</h1>
        <h2 className="my-title-text">
          I'm Sarah. A Full Stack Developer <br />
          based in Toronto.
        </h2>
        <HashLink to="#about">
          <button className="hero-button">LEARN MORE</button>
        </HashLink>
      </motion.div>
    </section>
  );
};

export default Hero;
