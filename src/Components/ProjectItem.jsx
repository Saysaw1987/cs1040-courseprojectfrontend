import React, { useEffect } from "react";
import { useAnimation, motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const animationVariant = {
  visible: {
    x: 0,
    opacity: 1,
    transition: { duration: 1.3 },
  },
  hidden: {
    x: -200,
    opacity: 0,
    transition: { duration: 1.3 },
  },
};

const ProjectItem = (props) => {
  const controls = useAnimation();
  const [ref, inView] = useInView({ threshold: 0.3 });

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    <motion.article
      ref={ref}
      animate={controls}
      initial="hidden"
      variants={animationVariant}
      className="project__card"
    >
      <div className="project__preview">
        <a href={props.demoHref} target="_blank" rel="noopener noreferrer">
          <img className="project__image" src={props.image} alt={props.alt} />
        </a>
      </div>
      <div className="project__desc">
        <h4>{props.projectTitle}</h4>
        <div className="projects-stack">{props.stackUsed}</div>
        <p className="project-desc-para">{props.description}</p>
        <a href={props.demoHref} target={"_blank"} className="med-blue-button">
          <FontAwesomeIcon
            icon="display"
            className="button-icon"
            title="Demo"
          />
          Demo
        </a>
        <a href={props.codeHref} target={"_blank"} className="dark-blue-button">
          <FontAwesomeIcon icon="code" className="button-icon" title="Repo" />
          Code
        </a>
      </div>
    </motion.article>
  );
};

export default ProjectItem;
