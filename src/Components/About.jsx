import React from "react";
import { Link } from "react-router-dom";
import profilePhoto from "../Assets/Images/sarah-photo.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const About = () => {
  return (
    <section className="about__section" id="about">
      <div className="about__container">
        <img
          className="profile__image"
          src={profilePhoto}
          alt="Profile of Sarah Salvatore"
        />
        <div className="about__text">
          <h3 id="about-link" className="sm-section-header">
            Intro
          </h3>
          <h2 className="section-title">About Me</h2>
          <p>
            Hello and welcome to my little corner of the internet! My name is
            Sarah Salvatore and I'm a Full Stack Developer living in beautiful
            Toronto, Ontario. If you're looking for a creative and passionate
            dev, you've come to the right place!
          </p>
          <p>
            I studied Full Stack Development at York University with a focus on
            the MERN stack. Before that, I graduated from George Brown College
            for Graphic Design. I would consider myself a forever student,
            always looking to learn new things, build on my programming
            knowledge and hone my skills.
          </p>
          <p className="about-paragraph">
            On a personal note, I love horror movies, coffee, and pineapple on
            pizza. When I'm not coding, you can usually find me hiking (or
            cuddling) with my two huskies. See below to learn more about me,
            check out samples of my work, or get in touch.
          </p>
        </div>
      </div>
      <div className="about-links">
        <Link
          to="./SSalvatoreResume.txt"
          target="_blank"
          className="resume-button"
          download
        >
          <FontAwesomeIcon
            icon="arrow-down"
            className="button-icon"
            title="Download CV"
          />
          Download CV
        </Link>
        <a
          href="https://www.linkedin.com/in/sarah-salvatore-75345b22a"
          target={"_blank"}
          className="med-blue-button"
        >
          <FontAwesomeIcon
            icon={["fab", "linkedin"]}
            className="button-icon"
            title="LinkedIn"
          />
          LinkedIn
        </a>
        <a
          href="https://github.com/SarahSalvatore"
          target={"_blank"}
          className="dark-blue-button"
        >
          <FontAwesomeIcon
            icon={["fab", "github"]}
            className="button-icon"
            title="GitHub"
          />
          GitHub
        </a>
      </div>
    </section>
  );
};

export default About;
