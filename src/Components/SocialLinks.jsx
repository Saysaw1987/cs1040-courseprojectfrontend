import React, { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SocialLinks = (props) => {
  return (
    <Fragment>
      <a href="mailto:sarah.h.salvatore@gmail.com">
        <FontAwesomeIcon
          icon="envelope"
          className={props.className}
          title="Email Me"
        />
      </a>
      <a
        href="https://www.linkedin.com/in/sarah-salvatore-full-stack-developer/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon
          icon={["fab", "linkedin"]}
          className={props.className}
          title="Stalk Me On LinkedIn"
        />
      </a>
      <a
        href="https://github.com/SarahSalvatore"
        target="_blank"
        rel="noopener noreferrer"
      >
        <FontAwesomeIcon
          icon={["fab", "github"]}
          className={props.className}
          title="See my GitHub"
        />
      </a>
    </Fragment>
  );
};

export default SocialLinks;
