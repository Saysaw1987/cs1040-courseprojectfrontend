import React from "react";

const EntryItem = (props) => {
  return (
    <div className="admin-entry-container">
      <div className="admin-grid">
        <h1 className="bold-key">Date Received:</h1>
        <p className="entry-data">{props.dateReceived}</p>
        <h1 className="bold-key">Form Id:</h1>
        <p className="entry-data">{props.id}</p>
        <h1 className="bold-key">Name:</h1>
        <p className="entry-data">{props.name}</p>
        <h1 className="bold-key">Email:</h1>
        <p className="entry-data">{props.email}</p>
        <h1 className="bold-key">Message:</h1>
        <p className="entry-data">{props.message}</p>
      </div>
      <div className="admin-button-container">
        <button
          className="crud-button"
          id="respond"
          onClick={props.responseOnClick}
        >
          Respond
        </button>
        <button
          className="crud-button"
          id="delete"
          onClick={props.deleteOnClick}
        >
          Delete
        </button>
      </div>
    </div>
  );
};

export default EntryItem;
