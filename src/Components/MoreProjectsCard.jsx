import React from "react";
import AnchorButton from "./AnchorButton";

const MoreProjectsCard = (props) => {
  return (
    <div className="more-projects-card-container">
      <div className="more-project-preview">
        <img
          className="more-projects-image"
          src={props.image}
          alt={props.alt}
        />
      </div>
      <div className="more-project-details">
        <h4 className="more-projects-title">{props.title}</h4>
        <p className="more-project-desc">{props.description}</p>
        <AnchorButton
          href={props.demoHref}
          className="demo-button"
          name="Demo"
          title="Demo"
        />
      </div>
    </div>
  );
};

export default MoreProjectsCard;
