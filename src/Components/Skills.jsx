import React, { useState } from "react";
import { LanguagesList, FrameworksList, OtherList } from "../SkillsList.js";
import SkillItem from "./SkillItem";

const Skills = () => {
  const [section1Visible, setSection1Visible] = useState(false);
  const [section2Visible, setSection2Visible] = useState(false);
  const [section3Visible, setSection3Visible] = useState(false);

  const toggle = (id) => {
    if (id === 1) {
      if (section1Visible) {
        setSection1Visible(false);
      } else {
        setSection1Visible(true);
        setSection2Visible(false);
        setSection3Visible(false);
      }
    }

    if (id === 2) {
      if (section2Visible) {
        setSection2Visible(false);
      } else {
        setSection2Visible(true);
        setSection1Visible(false);
        setSection3Visible(false);
      }
    }

    if (id === 3) {
      if (section3Visible) {
        setSection3Visible(false);
      } else {
        setSection3Visible(true);
        setSection1Visible(false);
        setSection2Visible(false);
      }
    }
  };

  return (
    <div className="skills-background">
      <div className="skills-container" onClick={() => toggle(1)}>
        <div className="skill-content">
          <div className="skill-header">
            Languages
            <span className="skill-toggle">{section1Visible ? "-" : "+"}</span>
          </div>
          <div className={section1Visible ? "skill-items" : "skill-hidden"}>
            {LanguagesList.map((item) => {
              return (
                <SkillItem
                  key={item.id}
                  icon={item.image}
                  alt={item.name}
                  title={item.name}
                />
              );
            })}
          </div>
        </div>
      </div>

      <div className="skills-container" onClick={() => toggle(2)}>
        <div className="skill-content">
          <div className="skill-header">
            Frameworks, Databases & Extensions
            <span className="skill-toggle">{section2Visible ? "-" : "+"}</span>
          </div>
          <div className={section2Visible ? "skill-items" : "skill-hidden"}>
            {FrameworksList.map((item) => {
              return (
                <SkillItem
                  key={item.id}
                  icon={item.image}
                  alt={item.name}
                  title={item.name}
                />
              );
            })}
          </div>
        </div>
      </div>
      <div className="skills-container" onClick={() => toggle(3)}>
        <div className="skill-content">
          <div className="skill-header">
            Additional Tools & Programs{" "}
            <span className="skill-toggle">{section3Visible ? "-" : "+"}</span>
          </div>
          <div className={section3Visible ? "skill-items" : "skill-hidden"}>
            {OtherList.map((item) => {
              return (
                <SkillItem
                  key={item.id}
                  icon={item.image}
                  alt={item.name}
                  title={item.name}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skills;
