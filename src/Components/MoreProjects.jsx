import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import ProjectListMinor from "../ProjectListMinor";
import MoreProjectsCard from "./MoreProjectsCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "swiper/swiper.min.css";

const MoreProjects = () => {
  return (
    <div className="more-projects-background">
      <div className="mp-width">
        <h1 className="sm-skills-header">More Projects</h1>
        <hr />
        <div className="more-projects-cards-container">
          <button className="slider-button" id="swiper-back">
            <FontAwesomeIcon
              icon="circle-arrow-left"
              className="slider-button-icon"
            />
          </button>
          <Swiper
            className="swiper"
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            breakpoints={{
              300: {
                slidesPerView: 1,
              },
              580: {
                slidesPerView: 1,
              },
              720: {
                slidesPerView: 2,
              },
              1200: {
                slidesPerView: 3,
              },
              1300: {
                slidesPerView: 4,
              },
            }}
            slidesPerView={4}
            navigation={{ nextEl: "#swiper-forward", prevEl: "#swiper-back" }}
            loop={true}
            scrollbar={{ draggable: true }}
          >
            {ProjectListMinor.map((project) => {
              return (
                <SwiperSlide className="slide" key={project.id}>
                  <MoreProjectsCard
                    key={project.id}
                    image={project.image}
                    title={project.title}
                    alt={project.alt}
                    description={project.description}
                    demoHref={project.demo}
                  />
                </SwiperSlide>
              );
            })}
          </Swiper>
          <button className="slider-button" id="swiper-forward">
            <FontAwesomeIcon
              icon="circle-arrow-right"
              className="slider-button-icon"
            />
          </button>
        </div>
      </div>
    </div>
  );
};

export default MoreProjects;
