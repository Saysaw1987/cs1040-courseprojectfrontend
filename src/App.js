import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { motion } from "framer-motion";
import Home from "./Pages/Home";
import Blog from "./Pages/Blog";
import Login from "./Pages/Login";
import Admin from "./Pages/Admin";
import CreateUser from "./Pages/CreateUser";
import EditUser from "./Pages/EditUser";
import SendMail from "./Pages/SendMail";
import Error from "./Pages/Error";
import { UserContext } from "./userContext";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faDisplay,
  faCode,
  faEnvelope,
  faX,
  faBars,
  faArrowRight,
  faCircleCheck,
  faCircleArrowLeft,
  faCircleArrowRight,
  faPaperPlane,
  faArrowDown,
  faPizzaSlice,
} from "@fortawesome/free-solid-svg-icons";
import "./CSS/App.css";

library.add(
  fab,
  faCode,
  faDisplay,
  faX,
  faEnvelope,
  faBars,
  faPaperPlane,
  faArrowRight,
  faArrowDown,
  faPizzaSlice,
  faCircleCheck,
  faCircleArrowLeft,
  faCircleArrowRight
);

function App() {
  const [user, setUser] = useState("user");

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
    >
      <Router>
        <UserContext.Provider value={{ user, setUser }}>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route path="/blog" element={<Blog />} />
            <Route path="*" element={<Error />} />
            <Route path="/login" element={<Login />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/admin/create-user" element={<CreateUser />} />
            <Route path="/admin/edit-user" element={<EditUser />} />
            <Route path="/admin/send-mail" element={<SendMail />} />
          </Routes>
        </UserContext.Provider>
      </Router>
    </motion.div>
  );
}

export default App;
