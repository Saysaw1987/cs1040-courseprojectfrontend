import htmlLogo from "./Assets/Images/html-logo.png";
import cssLogo from "./Assets/Images/css-logo.png";
import javascriptLogo from "./Assets/Images/javascript-logo.png";
import typescriptLogo from "./Assets/Images/typescript-logo.png";
import reactLogo from "./Assets/Images/react-logo.png";
import nodeLogo from "./Assets/Images/node-logo.png";
import expressLogo from "./Assets/Images/express-logo.png";
import mongoLogo from "./Assets/Images/mongo-logo.png";
import mySqlLogo from "./Assets/Images/mysql-logo.png";
import sassLogo from "./Assets/Images/sass-logo.png";
import illustratorLogo from "./Assets/Images/illustrator-logo.png";
import photoshopLogo from "./Assets/Images/photoshop-logo.png";
import gitLogo from "./Assets/Images/git-logo.png";

export const LanguagesList = [
  {
    id: 1,
    image: javascriptLogo,
    name: "JavaScript",
  },
  {
    id: 2,
    image: typescriptLogo,
    name: "TypeScript",
  },
  {
    id: 3,
    image: htmlLogo,
    name: "HTML",
  },
  {
    id: 4,
    image: cssLogo,
    name: "CSS",
  },
];

export const FrameworksList = [
  {
    id: 1,
    image: reactLogo,
    name: "React",
  },
  {
    id: 2,
    image: nodeLogo,
    name: "Node",
  },
  {
    id: 3,
    image: expressLogo,
    name: "Express",
  },
  {
    id: 4,
    image: sassLogo,
    name: "Sass",
  },
  {
    id: 5,
    image: mongoLogo,
    name: "MongoDB",
  },
  {
    id: 6,
    image: mySqlLogo,
    name: "mySQL",
  },
];

export const OtherList = [
  {
    id: 1,
    image: gitLogo,
    name: "Git",
  },
  {
    id: 2,
    image: photoshopLogo,
    name: "Photoshop",
  },
  {
    id: 3,
    image: illustratorLogo,
    name: "Illustrator",
  },
];
