import ToDoList from "./Assets/Images/mp-todolist.webp";
import Tetris from "./Assets/Images/mp-tetris.jpg";
import CurrencyConverter from "./Assets/Images/mp-currencyconvert.jpg";
import PacMan from "./Assets/Images/mp-pacman.jpg";
import ChatApp from "./Assets/Images/mp-chatapp.jpg";
import Calculator from "./Assets/Images/mp-calculator.jpg";

const ProjectListMinor = [
  {
    id: 1,
    title: "To Do List",
    image: ToDoList,
    alt: "project one preview",
    description: "Here are the project details.",
    demo: "https://github.com/SarahSalvatore",
  },
  {
    id: 2,
    title: "URL Shortener",
    image: Tetris,
    alt: "project two preview",
    description: "JavaScript, Node, MongoDB",
    demo: "https://github.com/SarahSalvatore",
  },
  {
    id: 3,
    title: "Currency Converter",
    image: CurrencyConverter,
    alt: "project three preview",
    description: "Here are the project details.",
    demo: "https://github.com/SarahSalvatore",
  },
  {
    id: 4,
    title: "Pac Man",
    image: PacMan,
    alt: "project four preview",
    description: "Here are the project details.",
    demo: "https://github.com/SarahSalvatore",
  },
  {
    id: 5,
    title: "Chat App",
    image: ChatApp,
    alt: "project five preview",
    description: "Here are the project details.",
    demo: "https://github.com/SarahSalvatore",
  },
  {
    id: 6,
    title: "Calculator",
    image: Calculator,
    alt: "project five preview",
    description: "Here are the project details.",
    demo: "https://github.com/SarahSalvatore",
  },
];

export default ProjectListMinor;
