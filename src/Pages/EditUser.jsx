import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const EditUser = () => {
  let navigate = useNavigate();
  let location = useLocation();

  const username = location.state.username;
  const [password, setPassword] = useState("");

  axios.defaults.withCredentials = true;
  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  const editUser = async (event) => {
    event.preventDefault();
    await axios.patch(`https://cloud-example-7dgxvgl46q-pd.a.run.app/admin`, {
      username: username,
      password: password,
    });
    alert("User credentials have been updated.");
    navigate("/admin");
  };

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <button className="admin-x-button" onClick={() => navigate("/admin")}>
          <FontAwesomeIcon icon="x" id="x" />
        </button>
        <h1 className="secondary-section-title">Edit User Password</h1>
        <form className="admin-form" onSubmit={editUser}>
          <div className="form-label-container">
            <label className="admin-form-label" htmlFor="username">
              Username:
            </label>
            <p className="admin-form-state" id="edit-username">
              {location.state.username}
            </p>
          </div>
          <div className="form-label-container">
            <label className="admin-form-label" htmlFor="password">
              New Password:
            </label>
            <input
              type="password"
              placeholder="Enter new password"
              name="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </div>
          <button className="admin-crud-button">Save</button>
        </form>
      </div>
    </div>
  );
};

export default EditUser;
