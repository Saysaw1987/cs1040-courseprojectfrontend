import React from "react";
import { useNavigate } from "react-router-dom";

const Blog = () => {
  let navigate = useNavigate();

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <h1 className="secondary-section-title">Blog Coming Soon!</h1>
        <button className="admin-crud-button" onClick={() => navigate("/")}>
          GO HOME
        </button>
      </div>
    </div>
  );
};

export default Blog;
