import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../userContext";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("*");
  const [passwordError, setPasswordError] = useState("*");
  const { setUser } = useContext(UserContext);

  axios.defaults.withCredentials = true;

  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  let navigate = useNavigate();

  useEffect(() => {
    if (!usernameError.length && !passwordError.length) {
      axios
        .post(`https://cloud-example-7dgxvgl46q-pd.a.run.app/login`, {
          username: username,
          password: password,
        })
        .then((response) => {
          setUsername(response.data.user);
          setUser(username);
          navigate("/admin", {
            state: { username },
          });
        })
        .catch((error) => {
          if (error) {
            console.log(error);
            alert(`Error: ${error.response.data.error}`);
          }
        });
    }
  }, [usernameError, passwordError]);

  const checkUsernameErrors = (username) => {
    if (!username) {
      setUsernameError("*Please enter your username.");
    } else {
      setUsernameError("");
    }
  };

  const checkPasswordErrors = (password) => {
    if (!password) {
      setPasswordError("*Please enter password.");
    } else if (password.length < 6) {
      setPasswordError("Password must be at least 6 characters.");
    } else {
      setPasswordError("");
    }
  };

  const loginUser = async (event) => {
    event.preventDefault();
    checkUsernameErrors(username);
    checkPasswordErrors(password);

    if (!usernameError.length && !passwordError.length) {
      axios
        .post(`https://cloud-example-7dgxvgl46q-pd.a.run.app/login`, {
          username: username,
          password: password,
        })
        .then((result) => {
          setUser(username);
          navigate("/admin");
        })
        .catch((error) => {
          if (error) {
            alert(`Error: ${error.response.data.error}`);
          }
        });
    }
  };

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <button className="admin-x-button" onClick={(event) => navigate("/")}>
          <FontAwesomeIcon icon="x" id="x" />
        </button>
        <h1 className="secondary-section-title">Login</h1>
        <form className="admin-form" onSubmit={loginUser}>
          <div>
            <label className="admin-form-label" htmlFor="username">
              Username:
            </label>
            <input
              type="text"
              placeholder="Enter username"
              name="username"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
            {usernameError.length > 1 ? (
              <p className="error-message" id="login-username">
                {usernameError}
              </p>
            ) : (
              <p></p>
            )}
          </div>
          <div>
            <label className="admin-form-label" htmlFor="password">
              Password:
            </label>
            <input
              type="password"
              placeholder="Enter password"
              name="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            {passwordError.length > 1 ? (
              <p className="error-message" id="login-password">
                {passwordError}
              </p>
            ) : (
              <p></p>
            )}
          </div>
          <button className="admin-crud-button">Login</button>
        </form>
      </div>
    </div>
  );
};

export default Login;
