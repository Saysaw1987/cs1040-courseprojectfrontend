import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faX } from "@fortawesome/free-solid-svg-icons";

const CreateUser = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  axios.defaults.withCredentials = true;
  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  let navigate = useNavigate();

  const createNewUser = async (event) => {
    event.preventDefault();
    await axios.post(`https://cloud-example-7dgxvgl46q-pd.a.run.app/admin`, {
      username: username,
      password: password,
    });
    alert("New user added");
    navigate("/admin");
  };

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <button
          className="admin-x-button"
          onClick={(event) => navigate("/admin")}
        >
          <FontAwesomeIcon icon={faX} id="x" />
        </button>
        <h1 className="secondary-section-title">Create New User</h1>
        <form className="admin-form" onSubmit={createNewUser}>
          <div className="form-label-container">
            <label className="admin-form-label" htmlFor="username">
              Username:
            </label>
            <input
              type="text"
              placeholder="Enter username"
              name="username"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
              required
            />
          </div>
          <div className="form-label-container">
            <label className="admin-form-label" htmlFor="password">
              Password:
            </label>
            <input
              type="password"
              placeholder="Enter password"
              name="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              required
            />
          </div>
          <button className="admin-crud-button">Register</button>
        </form>
      </div>
    </div>
  );
};

export default CreateUser;
