import React from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Error = () => {
  let navigate = useNavigate();

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <button className="admin-x-button" onClick={(event) => navigate("/")}>
          <FontAwesomeIcon icon="x" id="x" />
        </button>
        <h1 className="secondary-section-title">404 Error</h1>
        <p className="error-text">
          Hey there, whatcha looking for? <br />
          ...Whatever it was, it can't be found. Sorry about that.
        </p>
        <button className="admin-crud-button" onClick={() => navigate("/")}>
          GO HOME
        </button>
      </div>
    </div>
  );
};

export default Error;
