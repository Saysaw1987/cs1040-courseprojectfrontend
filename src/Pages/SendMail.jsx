import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SendMail = () => {
  const [subject, setSubject] = useState("");
  const [message, setMessage] = useState("");

  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  let navigate = useNavigate();
  let location = useLocation();

  const updateEntry = async (event) => {
    event.preventDefault();
    await axios
      .patch(
        `$https://cloud-example-7dgxvgl46q-pd.a.run.app/entries`,
        {
          message: message,
        },
        {
          withCredentials: true,
        }
      )
      .then((response) => {
        alert("Response sent!");
        navigate("/admin");
      })
      .catch((error) => {
        if (error) {
          alert(`Error: ${error.response.data.error}`);
        }
      });
  };

  return (
    <div className="secondary-admin-page">
      <div className="secondary-admin-container">
        <button
          className="admin-x-button"
          onClick={(event) => navigate("/admin")}
        >
          <FontAwesomeIcon icon="x" id="x" />
        </button>
        <h1 className="secondary-section-title">Send Email</h1>
        <form className="admin-form" onSubmit={updateEntry}>
          <div className="form-label-container">
            <p className="admin-form-label" id="to-box">
              To:
            </p>
            <div className="state-container">
              <p className="admin-form-state">{location.state.name}</p>
              <p className="admin-form-state">
                &#60;{location.state.email}&#62;
              </p>
            </div>
            <label className="admin-form-label" htmlFor="subject">
              Subject:
            </label>
            <input
              type="text"
              placeholder="Enter subject"
              name="subject"
              value={subject}
              onChange={(event) => setSubject(event.target.value)}
              required
            />
          </div>
          <div className="form-label-container">
            <label className="admin-form-label" htmlFor="message">
              Body:
            </label>
            <textarea
              placeholder="Enter message"
              name="message"
              value={message}
              onChange={(event) => setMessage(event.target.value)}
              required
            />
          </div>
          <button
            className="admin-crud-button"
            onClick={() => alert("Work in Progress.")}
          >
            Send
          </button>
        </form>
      </div>
    </div>
  );
};

export default SendMail;
