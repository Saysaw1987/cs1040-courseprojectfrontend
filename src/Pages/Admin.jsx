import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { UserContext } from "../userContext";
import EntryItem from "../Components/EntryItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUser, faGear, faLock } from "@fortawesome/free-solid-svg-icons";

const AdminPage = () => {
  const [entries, setEntries] = useState([]);
  const [users, setUsers] = useState([]);
  const [entryCount, setEntryCount] = useState(0);
  const [showAdminPanel, setAdminPanel] = useState(true);
  const { user } = useContext(UserContext);

  library.add(faUser, faGear, faLock);

  let navigate = useNavigate();

  axios.defaults.headers = {
    "Content-Type": "application/json",
  };

  useEffect(() => {
    getContactEntries();
    getAdminUsers();
  }, []);

  const getContactEntries = async () => {
    const response = await axios
      .get(`https://cloud-example-7dgxvgl46q-pd.a.run.app/entries`, {
        withCredentials: true,
      })
      .then((response) => {
        console.log(response.data);
        setEntries(response.data);
        setEntryCount(response.data.length);
      })
      .catch((error) => {
        if (error) {
          alert(`Error: ${error.response.data.error}`);
        }
      });
  };

  const deleteEntry = async (id) => {
    let confirmation = window.confirm(
      "Are you sure you want to delete this entry?"
    );

    if (confirmation) {
      await axios
        .delete(`https://cloud-example-7dgxvgl46q-pd.a.run.app/entries/${id}`, {
          withCredentials: true,
        })
        .then((response) => {
          alert("Entry has been deleted");
          getContactEntries();
        })
        .catch((error) => {
          if (error) {
            alert(`Error: ${error.response.data.error}`);
          }
        });
    }
  };

  const showPanel = () => {
    if (showAdminPanel === true) {
      setAdminPanel(false);
    } else {
      setAdminPanel(true);
    }
  };

  const getAdminUsers = async () => {
    const response = await axios
      .get(`https://cloud-example-7dgxvgl46q-pd.a.run.app/admin`, {
        withCredentials: true,
      })
      .then((response) => {
        setUsers(response.data);
        console.log(response.data);
      })
      .catch((error) => {
        if (error) {
          alert(`Error: ${error.response.data.error}`);
        }
      });
  };

  const deleteUser = async (id) => {
    let confirmation = window.confirm(
      "Are you sure you want to delete this admin user?"
    );

    if (confirmation) {
      await axios
        .delete(`https://cloud-example-7dgxvgl46q-pd.a.run.app/admin/${id}`, {
          withCredentials: true,
        })
        .then((response) => {
          alert("Admin user has been deleted");
          getAdminUsers();
        })
        .catch((error) => {
          if (error) {
            alert(`Error: ${error.response.data.error}`);
          }
        });
    }
  };

  const seeNewUserPage = () => {
    navigate("/admin/create-user");
  };

  const seeEditUserPage = (username) => {
    navigate("/admin/edit-user", {
      state: { username },
    });
  };

  const seeResponsePage = (id, name, email) => {
    navigate("/admin/send-mail", {
      state: { id, name, email },
    });
  };

  const logout = async () => {
    await axios.get(`https://cloud-example-7dgxvgl46q-pd.a.run.app/login`);
    navigate("/");
  };

  return (
    <div className="admin-page-container">
      <div className="admin-nav">
        <div className="admin-user-flex-start">
          <FontAwesomeIcon icon={"user"} className="button-icon" />
          <h1 className="admin-user-greeting">Welcome back, {user}!</h1>
        </div>
        <div className="admin-user-flex-end">
          <FontAwesomeIcon
            icon={"gear"}
            className="button-icon"
            id="gear"
            title="Admin Panel"
            onClick={showPanel}
          />
          <button className="nav-button" id="logout" onClick={logout}>
            LOGOUT
          </button>
        </div>
      </div>
      <div className="admin-background">
        <div className="admin-section">
          <h1 className="admin-section-title">Contact Form Entries</h1>
          <div className="metrics">
            <div className="metrics-text">
              You have <span className="metrics-count">{entryCount}</span>{" "}
              contact form entries.
            </div>
          </div>
          <div>
            {entries.map((entry) => {
              return (
                <EntryItem
                  key={entry.formId}
                  id={entry.formId}
                  name={entry.name}
                  email={entry.email}
                  message={entry.message}
                  dateReceived={entry.dateReceived.slice(0, 10)}
                  deleteOnClick={() => deleteEntry(entry.formId)}
                  responseOnClick={() =>
                    seeResponsePage(entry.formId, entry.name, entry.email)
                  }
                />
              );
            })}
          </div>
        </div>
        <div
          className={showAdminPanel === true ? "admin-panel" : "admin-hidden"}
        >
          <h1 className="admin-section-title" id="admin-panel-title">
            Admin Panel
          </h1>
          <div className="admin-panel-button-container">
            <button
              className="crud-button"
              id="create"
              onClick={seeNewUserPage}
            >
              Create New User
            </button>
          </div>
          {users.map((user) => {
            return (
              <div className="admin-user-container" key={user.userId}>
                <h1 className="bold-key">User Id:</h1>
                <p className="entry-data">{user.userId}</p>
                <h1 className="bold-key">Username:</h1>
                <p className="entry-data">{user.username}</p>
                <h1 className="bold-key">Password:</h1>
                <p className="entry-data">
                  <FontAwesomeIcon
                    icon={"lock"}
                    className="button-icon"
                    id="password-icon"
                  />
                </p>
                <button
                  className="crud-button"
                  id="edit"
                  onClick={() => seeEditUserPage(user.username)}
                >
                  Edit User
                </button>
                <button
                  className="crud-button"
                  id="delete"
                  onClick={() => deleteUser(user.userId)}
                >
                  Delete User
                </button>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default AdminPage;
